var mongoose = require("mongoose");
//mongoose.set("debug", true);

/* Declare schemas */
var Schema = mongoose.Schema;
var userSchema = new Schema({
  displayName: String,
  email: String,
  subscriptionCourseIds: [Number],
  dropboxToken: String
});

var fileSchema = new Schema({
  // introduce some kind of hash(filepath + filename)
  filename: String,
  filepath: String,
  timemodified: Number
});

var courseSchema = new Schema({
  courseId: Number,
  courseName: String,
  timemodified: Number,
  files: [fileSchema]
});

var User = mongoose.model("User", userSchema);
var File = mongoose.model("File", fileSchema);
var Course = mongoose.model("Course", courseSchema);

/*
 * TODO: No feedback if connection could be established successfully
 */
exports.connect = function() {
  var options = {
    server: {
      keepAlive: 1
    },
    replset: {
      socketOptions: {
        keepAlive: 1
      }
    }
  };
  mongoose.connect("mongodb://localhost/moodle_mongo", options);
};

exports.disconnect = function() {
  mongoose.disconnect();
};

exports.getUser = function(email, callback) {
  User.findOne({
    email: email
  }, function(err, user) {
    if (err) {
      callback(err);
    } else {
      callback(null, user ? user : new User({
        email: email,
        subscriptionCourseIds: []
      }));
    }
  });
};

exports.getCourse = function(courseId, callback) {
  Course.findOne({
    courseId: courseId
  }, function(err, course) {
    if (err) {
      callback(err);
    } else {
      callback(null, course ? course : new Course({
        courseId: courseId,
        courseName: "",
        timemodified: 0,
        files: []
      }));
    }
  })
};

exports.getNewFile = function(init_with) {
  return new File(init_with);
};

exports.getCoursesWithSubscribers = function(callback) {
  var o = {};
  o.map = function() {
    this.subscriptionCourseIds.forEach(function(id) {
      emit(id, 1);
    });
  };
  o.reduce = function(k, vals) {
    return vals.length;
  }
  User.mapReduce(o, callback);
};

function criterionCourseIdNotIn(courseids) {
  return {
    "courseId": {
      $nin: courseids
    }
  };
}

exports.getAllCoursesBut = function(courseids, callback) {
  Course.find(criterionCourseIdNotIn(courseids), callback);
};

exports.removeAllCoursesBut = function(courseids, callback) {
  Course.remove(criterionCourseIdNotIn(courseids), callback);
};

/* debug only */
exports.removeAllCourses = function(callback) {
  Course.remove({}, callback);
}
